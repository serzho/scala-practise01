package com.practise {

  import java.util.Date

  class Car(var model: String, var year: Int, var number: String) {

    var id: Int = 0
    var persisted: Boolean = false

    def this() = this(null, 0, null)

    override def toString() = List(this.id, this.model, this.number, this.year).mkString(", ")
  }
}
