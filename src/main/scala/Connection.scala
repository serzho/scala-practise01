package com.practise {

  import java.sql._

  class Connection private () {

  }

  object Connection {

    Class.forName("org.sqlite.JDBC")
    
    val connectionString = "jdbc:sqlite:scala.db"

    def update(sql: String, args: Any*) {

      val connection = DriverManager.getConnection(connectionString)

      try {
        val statement = connection.prepareStatement(sql)

        this.applyArgs(statement, args: _*)

        statement.execute
        statement.close

      } catch {
        case e: Exception => { 
          e.printStackTrace
        }
      } finally {
        try {
          connection.close
        } catch { case e => }
      }
    }

    def select(sql: String, args: Any*) (f: ResultSet => Car) = {

      val connection = DriverManager.getConnection(connectionString)

      try {

        val statement = connection.prepareStatement(sql)

        this.applyArgs(statement, args: _*)

        statement.execute
        val rs = statement.getResultSet

        var list = List[Car]()

        while (rs.next) {
          list = f(rs) +: list
        }

        rs.close
        statement.close
        
        list

      } catch {
        case e: Exception => { 
          e.printStackTrace
          List[Car]()
        }
      } finally {
        try {
          connection.close
        } catch { case e => }
      }
    }

    private def applyArgs(statement: PreparedStatement, args: Any*) {

      for (i <- 1 to args.length) {

        val arg = args(i-1)

        arg match {

          case int:Int => statement.setInt(i, int)
          case string:String => statement.setString(i, string)
        }
      }
    }
  }
}
