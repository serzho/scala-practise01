package com.practise {

  import java.sql.ResultSet

  class CarRepository private ()

  object CarRepository {

    def save(car: Car) { 

      if (car.persisted) {
        Connection.update("update cars set model = ?, year = ?, number = ? where id = ?",
          car.model, car.year, car.number, car.id)

      } else {
        Connection.update("insert into cars(model, year, number) values(?, ?, ?)",
          car.model, car.year, car.number)
        car.persisted = true
      }
    }

    def all = Connection.select("select * from cars") { rs => mapCar(rs) }

    def find_by_model(model: String) = find_by("model", model)

    def find_by_number(number: String) = find_by("number", number)

    def find_by_id(id: String) = find_by("id", id).head

    def delete(car: Car) {
      Connection.update("delete from cars where id = ?", car.id)
    }

    def deleteAll {
      Connection.update("delete from cars")
    }

    private def find_by(attribute: String, value: String) = {
      Connection.select("select * from cars where " + attribute + " = ?", value) { rs => mapCar(rs) }
    }

    private def mapCar(rs: ResultSet) : Car = {
    
        val car = new Car
        car.number = rs.getString("number")
        car.model = rs.getString("model")
        car.year = rs.getInt("year")
        car.id = rs.getInt("id")
        car.persisted = true
        car
    }
  }
}

