package com.practise {

  import org.scalatest.FlatSpec
  import org.scalatest.BeforeAndAfterEach
  import org.scalatest.matchers.ShouldMatchers

  class CarRepositorySpec extends FlatSpec with ShouldMatchers with BeforeAndAfterEach {

    var car: Car = _

    override def beforeEach() {

      CarRepository.deleteAll

      car = new Car
      car.model = "Peugeot"
      car.year = 2008
      car.number = "KP 0189"
    }

    it should "save car to db and retrieve it" in {
      
      CarRepository.save(car)
      val newCar = CarRepository.find_by_number("KP 0189").head

      newCar should not be (List())

      newCar.number should equal ("KP 0189")
      newCar.model should equal ("Peugeot")
      newCar.year should equal (2008)
    }

    it should "edit car and save it to db" in {
    
      CarRepository.save(car)

      val newCar = CarRepository.find_by_number("KP 0189").head
      newCar should not be (List())

      newCar.number = "KI 0011"
      newCar.model = "Lada"
      newCar.year = 2000

      newCar.persisted should equal (true)

      CarRepository.save(newCar)
      
      CarRepository.find_by_number("KP 0189") should be (List())

      val editedCar = CarRepository.find_by_number("KI 0011").head
      newCar should not be (List())

      newCar.number should equal ("KI 0011")
      newCar.model should equal ("Lada")
      newCar.year should equal (2000)
    }

    it should "find cars by model" in {
      
      val anotherCar = new Car
      anotherCar.model = "Peugeot"
      anotherCar.number = "KO 1098"
      anotherCar.year = 2009

      CarRepository.save(car)
      CarRepository.save(anotherCar)

      val cars = CarRepository.find_by_model("Peugeot")
      
      cars.size should equal (2)
      val result = cars.find( _.number == "KO 1098")

      result.isEmpty should be (false)

      val secondCar = cars.head

      secondCar.number should be ("KO 1098")
      secondCar.model should be ("Peugeot")
      secondCar.year should be (2009)
    }

    it should "delete car" in {
      
      CarRepository.save(car)

      val savedCar = CarRepository.find_by_number("KP 0189").head

      CarRepository.delete(savedCar)

      CarRepository.find_by_number("KP 0189") should be (List())
    }

    it should "retrieve all cars" in {
    
      CarRepository.save(car)

      val cars = CarRepository.all

      cars.size should be (1)

      val newCar = cars.head
      newCar.number should be ("KP 0189")
      newCar.model should be ("Peugeot")
      newCar.year should be (2008)
    }
  }
}
