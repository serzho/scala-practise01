name := "scala-practise01"

organization := "com.practise"

version := "1.0"

libraryDependencies += "org.xerial" % "sqlite-jdbc" % "3.6.20"

libraryDependencies += "org.scalatest" %% "scalatest" % "1.9.1" % "test"
